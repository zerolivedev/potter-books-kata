import { PotterBooks } from "../lib/PotterBooks"
import { expect } from "chai"

describe("PotterBooks", () => {
  it("does not apply any discount when buy a book", () => {
    const shoppingCart:Array<string> = ["Harry Potter y la piedra filosofal"]

    const totalPrice:number = PotterBooks.calculateDiscountFor(shoppingCart)

    const bookPrice:number = 8.00
    expect(totalPrice).to.eq(bookPrice)
  })

  it("does not apply any discount when buy the same book multiple times", () => {
    const shoppingCart:Array<string> = ["Harry Potter y la piedra filosofal", "Harry Potter y la piedra filosofal"]

    const totalPrice:number = PotterBooks.calculateDiscountFor(shoppingCart)

    expect(totalPrice).to.eq(16.00)
  })

  it("applies a discount when buy two different books", () => {
    const shoppingCart:Array<string> = ["Harry Potter y la cámara secreta", "Harry Potter y la piedra filosofal"]

    const totalPrice:number = PotterBooks.calculateDiscountFor(shoppingCart)

    expect(totalPrice).to.eq(15.20)
  })

  it("applies a discount when buy three different books", () => {
    const shoppingCart:Array<string> = [
      "Harry Potter y la cámara secreta",
      "Harry Potter y la piedra filosofal",
      "Harry Potter y el prisionero de Azkaban"
    ]

    const totalPrice:number = PotterBooks.calculateDiscountFor(shoppingCart)

    expect(totalPrice).to.eq(21.60)
  })

  it("applies a discount when buy four different books", () => {
    const shoppingCart:Array<string> = [
      "Harry Potter y la cámara secreta",
      "Harry Potter y la piedra filosofal",
      "Harry Potter y el prisionero de Azkaban",
      "Harry Potter y el cáliz de fuego"
    ]

    const totalPrice:number = PotterBooks.calculateDiscountFor(shoppingCart)

    expect(totalPrice).to.eq(25.6)
  })

  it("applies a discount when buy five different books", () => {
    const shoppingCart:Array<string> = [
      "Harry Potter y la cámara secreta",
      "Harry Potter y la piedra filosofal",
      "Harry Potter y el prisionero de Azkaban",
      "Harry Potter y el cáliz de fuego",
      "Harry Potter y la Orden del Fénix"
    ]

    const totalPrice:number = PotterBooks.calculateDiscountFor(shoppingCart)

    expect(totalPrice).to.eq(30.00)
  })

  it("applies a discount when buy two different books only", () => {
    const shoppingCart:Array<string> = [
      "Harry Potter y la cámara secreta", "Harry Potter y la piedra filosofal",
      "Harry Potter y la cámara secreta"
    ]

    const totalPrice:number = PotterBooks.calculateDiscountFor(shoppingCart)

    expect(totalPrice).to.eq(23.20)
  })

  it("applies different quantity discounts", () => {
    const shoppingCart:Array<string> = [
      "Harry Potter y la cámara secreta", "Harry Potter y la piedra filosofal",
      "Harry Potter y la cámara secreta",
      "Harry Potter y la piedra filosofal",
      "Harry Potter y el prisionero de Azkaban"
    ]

    const totalPrice:number = PotterBooks.calculateDiscountFor(shoppingCart)

    expect(totalPrice).to.eq(36.80)
  })

  it("applies different quantity discounts", () => {
    const shoppingCart:Array<string> = [
      "Harry Potter y la cámara secreta", "Harry Potter y la piedra filosofal",
      "Harry Potter y la cámara secreta",
      "Harry Potter y la piedra filosofal",
      "Harry Potter y el prisionero de Azkaban",
      "Harry Potter y la cámara secreta",
      "Harry Potter y la piedra filosofal",
      "Harry Potter y el prisionero de Azkaban",
      "Harry Potter y el cáliz de fuego",
      "Harry Potter y la Orden del Fénix"
    ]

    const totalPrice:number = PotterBooks.calculateDiscountFor(shoppingCart)

    expect(totalPrice).to.eq(66.80)
  })

  it("applies the same discounts", () => {
    const shoppingCart:Array<string> = [
      "Harry Potter y la cámara secreta", "Harry Potter y la piedra filosofal",
      "Harry Potter y la cámara secreta", "Harry Potter y la piedra filosofal"
    ]

    const totalPrice:number = PotterBooks.calculateDiscountFor(shoppingCart)

    expect(totalPrice).to.eq(30.40)
  })
})
