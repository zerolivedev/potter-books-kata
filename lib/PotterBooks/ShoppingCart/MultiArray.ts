
export class MultiArray {
  private values:Array<string>

  static fromCart(values:Array<string>):MultiArray {
    return new MultiArray(values)
  }

  constructor(values:Array<string>) {
    this.values = values
  }

  splitByUnique():Array<Array<string>> {
    const uniqueValues:Array<Array<string>> = []
    this.values.forEach(value => {
      if (this.areAllIncluding(uniqueValues, value)) {
        this.createNewListAndAdd(uniqueValues, value)
      } else {
        this.addToFirstWithout(uniqueValues, value)
      }
    })

    return uniqueValues
  }

  private addToFirstWithout(lists:Array<Array<string>>, value:string) {
    for(let values of lists) {
      if (!values.includes(value)) {
        values.push(value)
        break
      }
    }
  }

  private createNewListAndAdd(lists:Array<Array<string>>, value:string):void {
    const nextIndex = lists.length

    lists[nextIndex] = []
    lists[nextIndex]?.push(value)
  }

  private areAllIncluding(lists:Array<Array<string>> ,value:string):boolean {
    return lists.every(values => values.includes(value))
  }
}
