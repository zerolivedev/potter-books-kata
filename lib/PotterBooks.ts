import { ShoppingCart } from "./PotterBooks/ShoppingCart"
import { Discounts } from "./PotterBooks/Discounts"

export class PotterBooks {
  private static BOOK_PRICE:number = 8.00

  static calculateDiscountFor(books:Array<string>):number {
    const shoppingCart:ShoppingCart = new ShoppingCart(books)
    const totalPrice:number = this.BOOK_PRICE * shoppingCart.booksQuantity()
    let discountAmount:number = 0

    for(const quantity of Discounts.quantities()) {
      if(shoppingCart.hasDifferentBooks(quantity)) {
        const totalAmount:number = this.BOOK_PRICE * quantity
        const times = shoppingCart.countDifferentBooks(quantity)

        discountAmount += totalAmount * Discounts.forDifferentBooks(quantity) * times
      }
    }

    return totalPrice - discountAmount
  }
}
