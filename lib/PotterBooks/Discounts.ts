
export class Discounts {
  static quantities():Array<number> {
    const quantities = this.discounts().keys()

    return Array.from(quantities)
  }

  static forDifferentBooks(quantity:number):number {
    const discount = this.discounts().get(quantity)

    return discount || 0.0
  }

  private static discounts():Map<number, number> {
    const discounts = new Map<number, number>()

    discounts.set(2, 0.05)
    discounts.set(3, 0.10)
    discounts.set(4, 0.20)
    discounts.set(5, 0.25)

    return discounts
  }
}
