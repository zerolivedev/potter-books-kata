import { MultiArray } from "./ShoppingCart/MultiArray"

export class ShoppingCart {
  private cart:Array<string>

  constructor(cart:Array<string>) {
    this.cart = cart
  }

  booksQuantity():number {
    return this.cart.length
  }

  countDifferentBooks(quantity:number):number {
    const count = this.uniqueBooks().reduce((acc, list) => {
      if (list.length === quantity) { acc ++ }

      return acc
    }, 0)

    return count
  }

  hasDifferentBooks(quantity:number):boolean {
    return this.uniqueBooks().some(list => list.length === quantity)
  }

  private uniqueBooks():Array<Array<string>> {
    return MultiArray.fromCart(this.cart).splitByUnique()
  }
}
